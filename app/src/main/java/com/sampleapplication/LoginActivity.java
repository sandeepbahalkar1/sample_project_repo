    package com.sampleapplication;

    import android.content.Intent;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.support.v7.widget.Toolbar;
    import android.text.Editable;
    import android.text.Html;
    import android.text.TextUtils;
    import android.text.TextWatcher;
    import android.view.KeyEvent;
    import android.view.View;
    import android.view.View.OnClickListener;
    import android.view.inputmethod.EditorInfo;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.TextView;
    import android.widget.Toast;

    /**
     * A login screen that offers login via username/password.
     */
    public class LoginActivity extends AppCompatActivity {

        // UI references.
        private EditText muUserName;
        private EditText mPasswordView;
        private View mLoginFormView;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            // Set up the login form.

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle(getString(R.string.login_screen));
            muUserName = (EditText) findViewById(R.id.username);

//            muUserName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                @Override
//                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                    if (id == R.id.login || id == EditorInfo.IME_NULL) {
//                        attemptLogin();
//                        return true;
//                    }
//                    return false;
//                }
//            });

            muUserName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    attemptLogin();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            mPasswordView = (EditText) findViewById(R.id.password);
//            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                @Override
//                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                    if (id == R.id.login || id == EditorInfo.IME_NULL) {
//                        attemptLogin();
//                        return true;
//                    }
//                    return false;
//                }
//            });

            mPasswordView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                            attemptLogin();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            Button mLogInButton = (Button) findViewById(R.id.login_button);
            mLogInButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
                }
            });

        }





        /**
         * Attempts to sign in or register the account specified by the login form.
         * If there are form errors (invalid email, missing fields, etc.), the
         * errors are presented and no actual login attempt is made.
         */
        private void attemptLogin() {


            // Reset errors.
            muUserName.setError(null);
            mPasswordView.setError(null);

            // Store values at the time of the login attempt.
            String username = muUserName.getText().toString();
            String password = mPasswordView.getText().toString();




            boolean cancel = false;
            View focusView = null;


            // Check for a valid username address.
            if ((!TextUtils.isEmpty(username) && isPasswordValid(username)) || TextUtils.isEmpty(username)) {
                muUserName.setError(getString(R.string.error_short_username));
                muUserName.setError(Html.fromHtml("<font color='green'>Username should be at least 6 characters</font>"));
                focusView = muUserName;
                if (isUserNameValid(username)) {
                    muUserName.setError(getString(R.string.error_invalid_username));
                    muUserName.setError(Html.fromHtml("<font color='green'>This user name is invalid</font>"));
                    focusView = muUserName;

                }
                cancel = true;
            }
//            else if (isUserNameValid(username)) {
//                muUserName.setError(getString(R.string.error_invalid_username));
//                muUserName.setError(Html.fromHtml("<font color='green'>This user name is invalid</font>"));
//                focusView = muUserName;
//                cancel = true;
//            }

            // Check for a valid password, if the user entered one.
            else if ((!TextUtils.isEmpty(password) && isPasswordValid(password)) || TextUtils.isEmpty(password)) {
                mPasswordView.setError(getString(R.string.error_invalid_password));
                mPasswordView.setError(Html.fromHtml("<font color='green'>Password should be at least 6 characters</font>"));
                focusView = mPasswordView;
                cancel = true;
            }else{
                cancel=false;
            }


            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {

                String username1 = muUserName.getText().toString();
                if (isUserNameValid(username1)) {
                    muUserName.setError(getString(R.string.error_invalid_username));
                    muUserName.setError(Html.fromHtml("<font color='green'>This user name is invalid</font>"));
                    muUserName.requestFocus();

                }else {
                    // Show a progress spinner, and kick off a background task to
                    // perform the user login attempt.
                    Toast.makeText(LoginActivity.this, getString(R.string.user_logged_in), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("username", username);
                    startActivity(intent);
                }

            }
        }

        private boolean isUserNameValid(String email) {
            //TODO: Replace this with your own logic
            return email.contains("@");
        }

        private boolean isPasswordValid(String password) {
            //TODO: Replace this with your own logic
            return password.length() < 6;
        }



    }




